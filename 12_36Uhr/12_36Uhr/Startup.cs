﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_12_36Uhr.Startup))]
namespace _12_36Uhr
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
